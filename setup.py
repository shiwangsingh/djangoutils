from setuptools import setup

setup(
    name='djangoutils',
    version='0.0.1',
    description='a pip-installable package bdewutils',
    license='MIT',
    packages=['djangoutils'],
    author='Shiwang Singh',
    author_email='shiwang.singh84@gmail.com',
    keywords=['example'],
    url='https://github.com/cistox-tec/djangoutils',
)