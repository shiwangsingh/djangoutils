from django.core.mail import get_connection
from django.core import mail
from django.utils.log import AdminEmailHandler
from django.conf import settings


connection = get_connection(
    host=settings.MT_EMAIL_HOST,
    port=settings.MT_EMAIL_PORT,
    username=settings.MT_EMAIL_HOST_USER,
    password=settings.MT_EMAIL_HOST_PASSWORD
)


class CustomAdminEmailHandler(AdminEmailHandler):

    def send_mail(self, subject, message, *args, **kwargs):
        mail.mail_admins(subject, message, *args, connection=connection, **kwargs)